FROM ubuntu:14.04.4
MAINTAINER Ygor Barboza <ygorbarboza@gmail.com>

# Install java8
RUN apt-get update && apt-get install -y software-properties-common && apt-get clean && rm -fr /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN add-apt-repository -y ppa:webupd8team/java
RUN echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections
RUN apt-get update && apt-get install -y oracle-java8-installer && apt-get clean && rm -fr /var/lib/apt/lists/* /tmp/* /var/tmp/*

ENV ANDROID_COMPILE_SDK 25
ENV ANDROID_BUILD_TOOLS 25.0.0
ENV ANDROID_SDK_TOOLS 24.4.1
ENV ANDROID_EMULATOR_API_V21 21
ENV ANDROID_EMULATOR_API_V19 19

# Install dependencies
RUN apt-get install --yes --no-install-recommends \
    && apt-get --quiet update --yes --no-install-recommends \
    && apt-get --quiet install --no-install-recommends --yes wget tar unzip lib32stdc++6 lib32z1 python curl lib32ncurses5 lib32bz2-1.0 qemu-kvm libvirt-bin ubuntu-vm-builder bridge-utils \
    && rm -rf /var/lib/apt/lists/*

#Install KVM for hardware acceleration http://blog.whitehorses.nl/2015/03/07/enabling-hardware-acceleration-for-android-sdk-emulator-on-linux/
# RUN adduser ${USER} libvirtd
# RUN adduser ${USER} kvm

# Install SDK
RUN wget --quiet --output-document=android-sdk.tgz https://dl.google.com/android/android-sdk_r${ANDROID_SDK_TOOLS}-linux.tgz
RUN tar --extract --gzip --file=android-sdk.tgz

ENV ANDROID_HOME $PWD/android-sdk-linux
ENV PATH ${PATH}:${ANDROID_HOME}/tools:${ANDROID_HOME}/platform-tools
    
#Update Android dependencies 
RUN echo y | android update sdk --no-ui --all --filter android-${ANDROID_COMPILE_SDK}
RUN echo y | android update sdk --no-ui --all --filter platform-tools
RUN echo y | android update sdk --no-ui --all --filter build-tools-${ANDROID_BUILD_TOOLS}
RUN echo y | android update sdk --no-ui --all --filter extra-android-m2repository
RUN echo y | android update sdk --no-ui --all --filter extra-google-google_play_services
RUN echo y | android update sdk --no-ui --all --filter extra-google-m2repository
    
# Create Android 25 emulator
RUN echo y | android update sdk --no-ui --all --filter sys-img-x86-google_apis-${ANDROID_COMPILE_SDK}
RUN echo no | android create avd --abi google_apis/x86 -n emulator-${ANDROID_COMPILE_SDK} -t android-${ANDROID_COMPILE_SDK}

# Create Android 21 emulator
RUN echo y | android update sdk --no-ui --all --filter android-${ANDROID_EMULATOR_API_V21}
RUN echo y | android update sdk --no-ui --all --filter sys-img-x86-google_apis-${ANDROID_EMULATOR_API_V21}
RUN echo no | android create avd --abi google_apis/x86 -n emulator-${ANDROID_EMULATOR_API_V21} -t android-${ANDROID_EMULATOR_API_V21}

# Create Android 19 emulator
RUN echo y | android update sdk --no-ui --all --filter android-${ANDROID_EMULATOR_API_V19}
RUN echo y | android update sdk --no-ui --all --filter sys-img-x86-google_apis-${ANDROID_EMULATOR_API_V19}
RUN echo no | android create avd --abi google_apis/x86 -n emulator-${ANDROID_EMULATOR_API_V19} -t android-${ANDROID_EMULATOR_API_V19}
    
# Add wait for emulator function 
RUN wget --quiet --output-document=android-wait-for-emulator https://raw.githubusercontent.com/travis-ci/travis-cookbooks/0f497eb71291b52a703143c5cd63a217c8766dc9/community-cookbooks/android-sdk/files/default/android-wait-for-emulator
RUN chmod +x android-wait-for-emulator

# Support Gradle
ENV TERM dumb